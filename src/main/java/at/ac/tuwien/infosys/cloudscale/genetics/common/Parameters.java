package at.ac.tuwien.infosys.cloudscale.genetics.common;

import java.io.Serializable;

/**
 * Provide parameters for the Client.
 *
 * The only parameter is the population limit.
 */
public class Parameters implements Serializable {
    public static final int NUCLEOTIDE_BASES = 4650000;
    public static final double ELITISM_RATE = 0.9;
    public static final int MAX_TIME = 100;

    private SimulationParameters parameters;

    public Parameters(int populationLimit) {
        this.parameters = new SimulationParameters(ELITISM_RATE, MAX_TIME, populationLimit, NUCLEOTIDE_BASES);
    }

    public Parameters() {
    }

    public SimulationParameters getParameters() {
        return parameters;
    }

    public void setParameters(SimulationParameters parameters) {
        this.parameters = parameters;
    }
}
