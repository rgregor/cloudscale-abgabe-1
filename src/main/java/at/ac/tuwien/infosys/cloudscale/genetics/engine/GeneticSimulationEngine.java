package at.ac.tuwien.infosys.cloudscale.genetics.engine;

import at.ac.tuwien.infosys.cloudscale.annotations.CloudObject;
import at.ac.tuwien.infosys.cloudscale.annotations.DestructCloudObject;
import at.ac.tuwien.infosys.cloudscale.annotations.EventSink;
import at.ac.tuwien.infosys.cloudscale.genetics.common.NucleotideMutation;
import at.ac.tuwien.infosys.cloudscale.genetics.common.SimulationParameters;
import at.ac.tuwien.infosys.cloudscale.genetics.monitoring.CustomRAMEvent;
import at.ac.tuwien.infosys.cloudscale.genetics.util.GeneticsUtils;
import at.ac.tuwien.infosys.cloudscale.monitoring.IEventSink;
import at.ac.tuwien.infosys.cloudscale.server.CloudScaleServerRunner;
import org.apache.commons.math3.genetics.*;

import javax.jms.JMSException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * Simulates genetic evolution of {@link Population Populations}.
 */
@CloudObject
public class GeneticSimulationEngine {
    private SimulationParameters parameters;

    @EventSink
    private IEventSink events;

    public GeneticSimulationEngine() {}

    public GeneticSimulationEngine(SimulationParameters simulationParameters) {
        this.parameters = simulationParameters;
    }

    @Deprecated
    public void startMonitoring() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Runtime runtime = Runtime.getRuntime();
                long maxMemory = runtime.maxMemory() / 1024 / 1024;
                long usedMemory = (runtime.totalMemory() - runtime.freeMemory()) / 1024 / 1024;
                long freeMemory = maxMemory - usedMemory;
                String serverId = CloudScaleServerRunner.getInstance().getId().toString();
                try {
                    CustomRAMEvent event = new CustomRAMEvent(maxMemory, usedMemory, freeMemory, serverId);
                    events.trigger(event);
//                    System.out.println(event.toString());

                } catch (JMSException e) {
                    System.out.println("----- Triggering event failed!!!!");
                    e.printStackTrace();
                }
            }
        }, 50, 200);
    }

	/**
     * Runs a simulation using the provided {@link SimulationParameters}.
     *
     * @return the fittest chromosome of the population after the stopping condition is satisfied
     */
    @DestructCloudObject
	public String run() {
		GeneticAlgorithm ga = new GeneticAlgorithm(
				new OnePointCrossover<Integer>(), 1,
				new NucleotideMutation(), 1,
				new TournamentSelection(parameters.getPopulationLimit())
		);

		FixedElapsedTime condition = new FixedElapsedTime(parameters.getMaxTime(), TimeUnit.SECONDS);
		Population population = GeneticsUtils.createRandomPopulation(
				parameters.getPopulationLimit(), parameters.getNucleotides(), parameters.getElitismRate());
		Chromosome chromosome = ga.evolve(population, condition).getFittestChromosome();


		return chromosome.toString();
	}

    public SimulationParameters getParameters() {
        return parameters;
    }

}
