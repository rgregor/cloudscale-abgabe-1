package at.ac.tuwien.infosys.cloudscale.genetics.monitoring;

import at.ac.tuwien.infosys.cloudscale.messaging.objects.monitoring.Event;

/**
 * RAMEvent replacing the actual CloudScale RAMEvent because the built-in RAM
 * monitoring did not work.
 */
public class CustomRAMEvent extends Event {
    private String hostId;
    private double maxMemory;
    private double usedMemory;
    private double freeMemory;

    public CustomRAMEvent() {
    }

    public CustomRAMEvent(double maxMemory, double usedMemory, double freeMemory, String hostId) {
        this.maxMemory = maxMemory;
        this.usedMemory = usedMemory;
        this.freeMemory = freeMemory;
        this.hostId = hostId;
    }

    public String getHostId() {
        return hostId;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }

    public double getMaxMemory() {
        return maxMemory;
    }
    public void setMaxMemory(double maxMemory) {
        this.maxMemory = maxMemory;
    }
    public double getUsedMemory() {
        return usedMemory;
    }
    public void setUsedMemory(double usedMemory) {
        this.usedMemory = usedMemory;
    }
    public double getFreeMemory() {
        return freeMemory;
    }
    public void setFreeMemory(double freeMemory) {
        this.freeMemory = freeMemory;
    }

    @Override
    public String toString() {
        return "CustomRAMEvent{" +
                "hostId='" + hostId + '\'' +
                ", maxMemory=" + maxMemory +
                ", usedMemory=" + usedMemory +
                ", freeMemory=" + freeMemory +
                '}';
    }
}
