package at.ac.tuwien.infosys.cloudscale.genetics.engine;

import at.ac.tuwien.infosys.cloudscale.genetics.common.Parameters;

/**
 * The interface Clients should use to access the Genetic Simulation Engine
 */
public interface IClientSimulationEngine {
    String run(Parameters parameters);
}
