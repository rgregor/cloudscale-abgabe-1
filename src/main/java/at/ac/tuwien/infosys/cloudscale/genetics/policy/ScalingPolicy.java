package at.ac.tuwien.infosys.cloudscale.genetics.policy;

import at.ac.tuwien.infosys.cloudscale.genetics.common.SimulationParameters;
import at.ac.tuwien.infosys.cloudscale.genetics.engine.GeneticSimulationEngine;
import at.ac.tuwien.infosys.cloudscale.monitoring.EventCorrelationEngine;
import at.ac.tuwien.infosys.cloudscale.monitoring.MonitoringMetric;
import at.ac.tuwien.infosys.cloudscale.policy.IScalingPolicy;
import at.ac.tuwien.infosys.cloudscale.vm.ClientCloudObject;
import at.ac.tuwien.infosys.cloudscale.vm.IHost;
import at.ac.tuwien.infosys.cloudscale.vm.IVirtualHostPool;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

public class ScalingPolicy implements IScalingPolicy {
    public static final int HOST_LIMIT = 4;
    private final ReentrantLock lock = new ReentrantLock();

    @Override
    public IHost selectHost(ClientCloudObject newCloudObject, IVirtualHostPool hostPool) {
        // Run this method mutually exclusively!
        lock.lock();

        try {
        /* I'd like to inspect the parameters of the ClientCloudObject here and take a decision
           based on the parameters. Unfortunately, that does not work (Internal CloudScale Exception)
         */
//        SimulationParameters parameters = getParametersFromCloudObject(newCloudObject);

            int hostsCount = 0;
            for (IHost host : hostPool.getHosts()) {
                if(host.isOnline() || (!host.isOnline() && host.getStartupTime() == null)) {
                    hostsCount++;
                }
            }

            System.out.printf("At the moment there are so many hosts: %d%n", hostsCount);
            IHost selectedHost = null;

            /**
             * If hostsCount == 0:
             *      Start two new hosts. One of them should execute the newCloudObject
             * Otherwise:
             *      If there is a host with no running job, take that one.
             *      Otherwise:
             *          If there is a host with one running job, take that one.
             *          Otherwise:
             *              If hostsCount == HOST_LIMIT then return null
             *              Start a completely new host.
             */

            if(hostsCount == 0) {

                /*
                    I used to monitor the maximally used memory of the last 120 seconds to get an idea of the load.
                    As the memory usage differed greatly between executions with the same population limit parameter (because of the randomness of the algorithm?!)
                    it didn't make much sense to me making a decision based on that stat.
                 */
    //          registerMaxUsedMemory(newHost.getId().toString());

                // Start a reserve host
//                hostPool.startNewHostAsync();
                return hostPool.startNewHost();
            } else {
                Multimap<Integer, IHost> nrOfJobsToUUIDs = ArrayListMultimap.create();
                for (IHost host : hostPool.getHosts()) {
                    nrOfJobsToUUIDs.put(host.getCloudObjectsCount(), host);
                }
                Map<Integer,Collection<IHost>> nrOfJobsToUUIDsAsMap = nrOfJobsToUUIDs.asMap();
                if(nrOfJobsToUUIDsAsMap.containsKey(0)) {
                    selectedHost = nrOfJobsToUUIDsAsMap.get(0).iterator().next();
                    if(hostsCount + 1 <= HOST_LIMIT && nrOfJobsToUUIDsAsMap.get(0).size() == 1) {
                        hostPool.startNewHostAsync();
                    }
                } else if(nrOfJobsToUUIDsAsMap.containsKey(1)) {
                    selectedHost = nrOfJobsToUUIDsAsMap.get(1).iterator().next();
                    if(hostsCount + 1 <= HOST_LIMIT) hostPool.startNewHostAsync();
                } else {
                    if(hostsCount >= HOST_LIMIT) {
                        String excuse = "Not enough resources! Issue your request later, please!";
                        System.out.println(excuse);
                        throw new RuntimeException(excuse);
                    }
                    selectedHost = hostPool.startNewHost();
                }
            }

            return selectedHost;
        } catch(RuntimeException e) {
            throw e;
        }
        catch(Exception e) {
            System.out.println("Exception in selectHost!");
            e.printStackTrace();
        } finally {
            lock.unlock();
        }

        return null;
    }

    @Override
    public boolean scaleDown(IHost host, IVirtualHostPool hostPool) {
        try {
            boolean scaleDown = true;
            System.out.printf("Checking whether to scale down host %s%n", host.getId().toString());

            if(! host.isOnline()) {
                scaleDown = false;
                System.out.println("Don't scale down. " + host.getId() + " is offline");
            }
            else if(host.getCloudObjectsCount() > 0) {
                scaleDown = false;
                System.out.println("Don't scale down. " + host.getId() + " is in use");
            }
            else if(noOtherUnusedHostAvailable(hostPool, host.getId())) {
                scaleDown = false;
                System.out.println("Don't scale down. " + host.getId() + " is the only unused host");
            }

            if(scaleDown) {
                System.out.printf("Scale down host %s%n", host.getId().toString());
    //            unregisterMaxUsedMemory(host.getId().toString());
            }

            return scaleDown;
        } catch (Exception e) {
            System.out.println("Exception in scaleDown!");
            e.printStackTrace();
        }

        return false;
    }

    @Deprecated
    private String getMaxUsedMemory(String serverId) {
        return "maxUsedMemory_" + serverId;
    }

    /**
     *
     * @param hostPool
     * @param id the id of the host which should be checked to be the only unused host
     * @return true iff the host of the id is the only unused host atm, false otherwise.
     */
    private boolean noOtherUnusedHostAvailable(IVirtualHostPool hostPool, UUID id) {
        try {
            for(IHost host : hostPool.getHosts()) {
                if(! host.getId().equals(id) && host.getCloudObjectsCount() == 0) {
                    return false;
                }
            }
        } catch (Exception e) {
            System.out.println("Exception in noOtherUnusedHostAvailable"); }
        return true;
    }
    /**
     *
     * @param newCloudObject the CloudObject which should be deployed on a host
     * @return the SimulationParameters for this CloudObject (which obviously should be a GeneticSimulationEngine
     */
    private SimulationParameters getParametersFromCloudObject(ClientCloudObject newCloudObject) {
        GeneticSimulationEngine engine = ((GeneticSimulationEngine) newCloudObject.getProxy());
        return engine.getParameters();
    }

    @Deprecated
    private void registerMaxUsedMemory(String serverId) {
        MonitoringMetric metric = new MonitoringMetric();
        String name = getMaxUsedMemory(serverId);
        metric.setName(name);
        metric.setEpl(
                String.format(
                        "select max(usedMemory) as maxUsed from CustomRAMEvent(hostId=\"%s\").win:time(120 sec)",
                        serverId)
        );
        EventCorrelationEngine.getInstance().registerMetric(metric);
    }

    @Deprecated
    private double getUsedMemory(String serverId) {
        Object value =
                EventCorrelationEngine.getInstance().getMetricsDatabase().getLastValue(getMaxUsedMemory(serverId));
        if(value == null) {
            return -1;
        }
        else {
            String maxUsed = "maxUsed";
            if (((HashMap) value).get(maxUsed) == null) {
                return 0;
            }
            else {
                return (Double) ((HashMap) value).get(maxUsed);
            }
        }
    }

    @Deprecated
    private void unregisterMaxUsedMemory(String serverId) {
        EventCorrelationEngine.getInstance().unregisterMetric(getMaxUsedMemory(serverId));
    }
}
