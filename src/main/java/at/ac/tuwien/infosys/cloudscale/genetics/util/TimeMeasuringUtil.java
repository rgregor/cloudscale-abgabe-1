package at.ac.tuwien.infosys.cloudscale.genetics.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TimeMeasuringUtil {
    private Map<String, Long> startTimes =  new HashMap<>();

    public void start(String clientId) {
        startTimes.put(clientId, new Date().getTime());
    }

    public long getRuntime(String clientId) {
        long now = new Date().getTime();
        long runtime = now - startTimes.get(clientId);

        return runtime / 1000;
    }
}
