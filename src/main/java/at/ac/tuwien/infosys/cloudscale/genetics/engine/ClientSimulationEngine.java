package at.ac.tuwien.infosys.cloudscale.genetics.engine;

import at.ac.tuwien.infosys.cloudscale.genetics.common.Parameters;
import at.ac.tuwien.infosys.cloudscale.genetics.common.SimulationParameters;

/**
 * Makes sure only requests meeting the population limit restriction are permitted.
 */
public class ClientSimulationEngine implements IClientSimulationEngine {
    @Override
   public String run(Parameters parameters) {
       SimulationParameters simulationParameters = parameters.getParameters();
       if(simulationParameters.getPopulationLimit() > 25) {
           System.out.println("The cloud can only execute Genetic Simulation Requests up to a population limit of 25. Please issue only requests smaller than or equal to a population limit of 25!");
           return "";
       }
       GeneticSimulationEngine simulationEngine = new GeneticSimulationEngine(simulationParameters);
       return simulationEngine.run();
   }
}
