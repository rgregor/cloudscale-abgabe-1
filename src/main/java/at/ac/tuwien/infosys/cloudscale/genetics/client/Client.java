package at.ac.tuwien.infosys.cloudscale.genetics.client;

import at.ac.tuwien.infosys.cloudscale.annotations.CloudScaleConfigurationProvider;
import at.ac.tuwien.infosys.cloudscale.annotations.CloudScaleShutdown;
import at.ac.tuwien.infosys.cloudscale.configuration.CloudScaleConfiguration;
import at.ac.tuwien.infosys.cloudscale.configuration.CloudScaleConfigurationBuilder;
import at.ac.tuwien.infosys.cloudscale.genetics.common.Parameters;
import at.ac.tuwien.infosys.cloudscale.genetics.engine.ClientSimulationEngine;
import at.ac.tuwien.infosys.cloudscale.genetics.engine.IClientSimulationEngine;
import at.ac.tuwien.infosys.cloudscale.genetics.monitoring.CustomRAMEvent;
import at.ac.tuwien.infosys.cloudscale.genetics.policy.ScalingPolicy;
import at.ac.tuwien.infosys.cloudscale.genetics.util.TimeMeasuringUtil;
import at.ac.tuwien.infosys.cloudscale.vm.ICloudPlatformConfiguration;
import at.ac.tuwien.infosys.cloudscale.vm.openstack.OpenstackCloudPlatformConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

public class Client {
    @CloudScaleShutdown
	public static void main(String... args) throws Exception {
        final TimeMeasuringUtil timeMeasuringUtil = new TimeMeasuringUtil();
        if(args.length != 2) System.out.println("USAGE: mvn exec:exec -Dexec.args=REQUESTNAME POPULATIONLIMIT");

//        String request = args[0];
//        int populationLimit = Integer.parseInt(args[1]);
        String request = "request";
        int populationLimit = 20;

        runSimulation(populationLimit, request, timeMeasuringUtil);
    }

    private static void runSimulation(final int populationLimit, final String request, final TimeMeasuringUtil timeMeasuringUtil) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                IClientSimulationEngine engine = new ClientSimulationEngine();
                timeMeasuringUtil.start(request);
                String result = engine.run(new Parameters(populationLimit));

                System.out.println("---");
                System.out.println(request + ": " + result.substring(0, 10) + " etc.");
                System.out.println("All in all " + request + " took " + timeMeasuringUtil.getRuntime(request));
                System.out.println("---");
            }
        }).start();
    }

    // this method is used to specify configuration for CloudScale.
    @CloudScaleConfigurationProvider
    public static CloudScaleConfiguration getConfiguration() throws IOException {
        CloudScaleConfiguration cfg = CloudScaleConfigurationBuilder
//                .createOpenStackConfigurationBuilder("openstack.props", "128.130.172.227", "HostImage10")
                .createOpenStackConfigurationBuilder("openstack.props", "10.99.0.135", "HostImage10")
                .with(new ScalingPolicy())
                .withMonitoring(true)
                .withMonitoringEvents(CustomRAMEvent.class)
                .withGlobalLoggingLevel(Level.CONFIG).build();
        cfg.common().setScaleDownIntervalInSec(15);

        ICloudPlatformConfiguration platform = cfg.server().cloudPlatform();
        OpenstackCloudPlatformConfiguration osPlatform = ((OpenstackCloudPlatformConfiguration) platform);
        osPlatform.setNewInstanceType("m1.medium");

        cfg.save(new File("config.xml"));
        return cfg;


    }
}
